"use strict";
var builder = require("botbuilder");
var loki = require("lokijs");
var botbuilder_azure = require("botbuilder-azure");
var useEmulator = false;

var path = require('path');
var connector = useEmulator ? new builder.ChatConnector() : new botbuilder_azure.BotServiceConnector({
	appId: process.env['MicrosoftAppId'],
	appPassword: process.env['MicrosoftAppPassword'],
	stateEndpoint: process.env['BotStateEndpoint'],
	openIdMetadata: process.env['BotOpenIdMetadata']
});
//end of base configuration

var db = new loki('database.db');
var users = db.addCollection('users');
var raids = db.addCollection('raids');
var HelpMessage = 'Ik kan het volgend voor je doen: \r\n\r\n' +
    '- /nieuw | meld een nieuwe Raid. Alle gebruikers krijgen automatisch een berichtje.\r\n\r\n' +
    '- /raids | ontvang een lijst met alle gemeldde Raids. Hierna kan je ook opnieuw aangeven of je komt of niet.\r\n\r\n' +
	'- /annuleren | laat een Raid niet door gaan, bijvoorbeeld omdat er te weinig opkomst is. \r\n\r\n' +
    '- /help | Toon dit lijstje met commando\'s, voor het geval je iets vergeet.\r\n\r\n' +
    '- /reset | meld je af voor deze bot. Zo kan je ook je username veranderen.\r\n\r\n';
var RaidBosses = ["Snorlax", "Tyranitair", "Articuno", "Zapdos", "Moltres", "Mewtwo", "Mew", "Raikou", "Eintei", "Suicune", "Lugia", "Ho-Oh", "Celebi"];
var RaidLocations = [
	["Apostolische Kerk", "https://www.google.com/maps/dir/Current+Location/52.719524,6.450944"],
	["Arrow To The Sky", "https://www.google.com/maps/dir/Current+Location/52.719542,6.491674"],
	["Artsy Wall", "https://www.google.com/maps/dir/Current+Location/52.726188,6.486566"],
	["Basketbalveld", "https://www.google.com/maps/dir/Current+Location/52.719772,6.448395"],
	["Bedriegertjes", "https://www.google.com/maps/dir/Current+Location/52.720764,6.478371"],
	["Bentincspark", "https://www.google.com/maps/dir/Current+Location/52.720856,6.495597"],
	["Buizerd", "https://www.google.com/maps/dir/Current+Location/52.715531,6.498426"],
	["Buurtpark Zeehelden-", "https://www.google.com/maps/dir/Current+Location/52.718447,6.474103"],
	["Cascade Sculptuur-", "https://www.google.com/maps/dir/Current+Location/52.724425,6.477664"],
	["De Bron", "https://www.google.com/maps/dir/Current+Location/52.726276,6.460012"],
	["De Kerk op de Roton-", "https://www.google.com/maps/dir/Current+Location/52.738906,6.485071"],
	["De Koppeling", "https://www.google.com/maps/dir/Current+Location/52.725512,6.464619"],
	["De Lichtnaald", "https://www.google.com/maps/dir/Current+Location/52.72371,6.463277"],
	["De Opgang", "https://www.google.com/maps/dir/Current+Location/52.71744,6.477629"],
	["De Vredehorst Kerk", "https://www.google.com/maps/dir/Current+Location/52.718989,6.454267"],
	["Fonteinen Hoofdstra-", "https://www.google.com/maps/dir/Current+Location/52.720714,6.478862"],
	["Gemeentehuis Hoogev-", "https://www.google.com/maps/dir/Current+Location/52.726231,6.474919"],
	["Glazen Kies", "https://www.google.com/maps/dir/Current+Location/52.737501,6.48209"],
	["Graffiti Park Zuid", "https://www.google.com/maps/dir/Current+Location/52.711683,6.479865"],
	["Het Apostolisch Gen-", "https://www.google.com/maps/dir/Current+Location/52.723282,6.471852"],
	["Het Land Werd Bequa-", "https://www.google.com/maps/dir/Current+Location/52.714671,6.436948"],
	["Hunebed Hoogeveen", "https://www.google.com/maps/dir/Current+Location/52.736774,6.471983"],
	["Ichtus Kerk", "https://www.google.com/maps/dir/Current+Location/52.713612,6.490882"],
	["Maxx Sports and Eve-", "https://www.google.com/maps/dir/Current+Location/52.727322,6.501096"],
	["Oosterkerk", "https://www.google.com/maps/dir/Current+Location/52.719383,6.507152"],
	["Oude Klooster", "https://www.google.com/maps/dir/Current+Location/52.731147,6.477531"],
	["Pierebadje", "https://www.google.com/maps/dir/Current+Location/52.723608,6.513649"],
	["Podium Cascade", "https://www.google.com/maps/dir/Current+Location/52.723482,6.477732"],
	["Promens Care", "https://www.google.com/maps/dir/Current+Location/52.724745,6.487165"],
	["Spartelvijver Kids", "https://www.google.com/maps/dir/Current+Location/52.721813,6.457018"],
	["Speeltuin Bij Vlind-", "https://www.google.com/maps/dir/Current+Location/52.71343,6.448195"],
	["Speeltuin Pallazzo", "https://www.google.com/maps/dir/Current+Location/52.721171,6.450766"],
	["Sportpark Keizersma-", "https://www.google.com/maps/dir/Current+Location/52.71026,6.448248"],
	["Station Hoogeveen", "https://www.google.com/maps/dir/Current+Location/52.733576,6.473217"],
	["Stichting Salawaku", "https://www.google.com/maps/dir/Current+Location/52.718452,6.463103"],
	["Trommelslager", "https://www.google.com/maps/dir/Current+Location/52.725546,6.480204"],
	["Turfgravers", "https://www.google.com/maps/dir/Current+Location/52.726273,6.472425"],
	["VV Hoogeveen", "https://www.google.com/maps/dir/Current+Location/52.723395,6.495791"],
	["Velden van Nevel", "https://www.google.com/maps/dir/Current+Location/52.707772,6.447399"],
	["Wijkgemeente Weide", "https://www.google.com/maps/dir/Current+Location/52.716495,6.442706"],
	["Woning Vincent v Go-", "https://www.google.com/maps/dir/Current+Location/52.733471,6.476388"],
	["XXL Bike", "https://www.google.com/maps/dir/Current+Location/52.722372,6.467032"],
	["Noordseschut", "https://www.google.com/maps/dir/Current+Location/52.721509,6.534252"],
	["Hollandscheveld", "https://www.google.com/maps/dir/Current+Location/52.701053,6.533695"],
	["Echten", "https://www.google.com/maps/dir/Current+Location/52.713621,6.398679"],
	["Pesse", "https://www.google.com/maps/dir/Current+Location/52.77001,6.450837"]
];
var Admins = ["Richh94", "BellenBlaasBoss", "BellenBlaasBaas", "Weszzj"];
var RaidConfirmChoices = ["Ja", "Nee"];
var RaidComingChoices = ["Ja", "Nee"];

var bot = new builder.UniversalBot(connector, function(session) {
    //first check if we know this user: if there is a conversation.
    var user = users.find({"id": session.message.address.conversation.id});
    if (user.length < 1) {
        return session.beginDialog('greet');
    }
});
//here is all the shit saved
bot.set('persistConversationData', true);

bot.dialog('greet', new builder.SimpleDialog(function (session, results) {
    if (results && results.response) {
        var address = session.message.address;
        users.insert({
            "id": address.conversation.id,
            "address": address,
            "name": results.response,
            "notify": true});
        if(isGroup(results.response)){
            return session.endDialog(
                'Deze groep is nu aangemeld voor notificaties van @PokemonGoRaidsHoogeveenBot. Voeg mij persoonlijk toe voor interactie.');
        }
        return session.endDialog(
            'Hey '+results.response+'!\r\n\r\n' +
            ' \r\n\r\n' +
            'Vanaf nu krijg je automatisch een berichtje als er een Raid in de buurt is! \r\n\r\n' +
            ' \r\n\r\n' +
            '' + HelpMessage + '\r\n\r\n' +
            ' \r\n\r\n' +
            'Type nu /raids om te kijken of er op dit moment Raids zijn aangemeld.');
    }

    builder.Prompts.text(session,
        'Hey, welkom bij de Pokémon Go Hoogeveen Raid Bot. ' +
        'Door Raids via deze bot te organiseren, hopen we dat we alles wat overzichtelijker houden! \r\n\r\n' +
        'Met deze bot kan je straks het volgende doen:\r' +
        '- Een berichtje ontvangen als er een Raid in de buurt is \r' +
        '- Aangeven of je aanwezig bent (of niet) bij een Raid \r' +
        '- Nieuwe Raids aanmelden, waar anderen een bericht van krijgen \r\n\r\n' +
        'Hoe het werkt leg ik zometeen uit. \r\n\r\n ' +
        '\r\n\r\n' +
        'Eerst: wat is je Pokémon Go username?');
}));

function getGmapsLocation(locationName) {
	var value = "";
	RaidLocations.forEach(function (raid) {
		if(raid[0] === locationName){
			value = raid[1];
		}
	});
	return value;
}

var raidPokemon;
var raidLocation;
var raidTime;
var senderUserName;
var dbId;
bot.dialog('addRaid', [
    function(session){
		var user = users.findOne({"id": session.message.address.conversation.id});
        if(isGroup(user.name)){
            return;
        }
        builder.Prompts.choice(session, "We gaan een nieuwe Raid toevoegen! Welke Pokémon is de Raid Boss?", RaidBosses);
    },
    function(session, results){
        raidPokemon = results.response.entity;
		var raidLocations = [];
		RaidLocations.forEach(function (raid) {
			raidLocations.push(raid[0]);
		})
        builder.Prompts.choice(session, "Waar is de Raid?", raidLocations);
    },
    function(session, results){
        raidLocation = results.response.entity;
        builder.Prompts.text(session, "Hoe laat stel je voor om te beginnen aan de Raid? Typ je tijd als bijvoorbeeld: _16:45_ ");
    },
    function(session, results){
        raidTime = results.response;
		var user = users.findOne({"id": session.message.address.conversation.id});
        builder.Prompts.choice(session, "Klaar met instellen! Ik ga nu iedereen een berichtje sturen met de tekst: \r\n\r\n" +
            "Hey, " + user.name + " heeft een Raid gevonden. \r\n\r\n" +
            "- Pokémon: "+raidPokemon+"\r\n\r\n" +
            "- Locatie: "+raidLocation+"\r\n\r\n" +
            "- Starttijd: "+raidTime +"\r\n\r\n" +
			"- " + getGmapsLocation(raidLocation) + "\r\n\r\n" +
            "Is dit oke?", RaidConfirmChoices);
    },
    function(session, results){
        if(results.response.entity === RaidConfirmChoices[0]){
            var date = new Date();
            var insertObj = raids.insert({
                "date-year": date.getFullYear(),
                "date-month": date.getMonth(),
                "date-day": date.getDate(),
                "date-hour": date.getHours(),
                "date-minute": date.getMinutes(),
                "pokemon": raidPokemon,
                "location": raidLocation,
                "raidTime": raidTime,
                "userId": session.message.address.conversation.id,
                "coming": []
            });
			var user = users.findOne({"id": session.message.address.conversation.id});
            senderUserName = user.name;
            dbId = insertObj.$loki;
            session.endDialog("" +
                "Dank voor het melden van deze "+raidPokemon+"-Raid! " +
                "Iedereen krijgt nu een berichtje en kan daarin aangeven of hij of zij komt. " +
                "Jijzelf ook, antwoord hieronder of hierboven!:");
            sendEverybodyRaidNotification();
        }else{
            session.endDialog("Oke, het toevoegen van een nieuwe Raid is gestopt. Om opnieuw te beginnen, type /nieuw.")
        }
    }
]).triggerAction({ matches: /^\/nieuw/i});

function sendEverybodyRaidNotification(){
    var userResult = users.find({"notify": true});
    userResult.forEach(function(user){
        var address = user.address;
        if(isGroup(user.name)){
            bot.beginDialog(address, '*:comingToRaidGroup');
        } else {
            bot.beginDialog(address, "*:comingToRaid");
        }
    });
}

bot.dialog('comingToRaid', [
    function(session){
        builder.Prompts.choice(session, "Hey, " + senderUserName + " heeft een Raid gevonden. \r\n\r\n" +
            "- Pokémon: "+raidPokemon+"\r\n\r\n" +
            "- Locatie: "+raidLocation+"\r\n\r\n" +
            "- Starttijd: "+raidTime + "\r\n\r\n" +
			"- " + getGmapsLocation(raidLocation) + "\r\n\r\n" +
            "Kom je ook?", RaidComingChoices);
    },
    function(session, results){
		if(results.response.entity === RaidComingChoices[0]){
			var thisRaid = raids.findOne({"$loki": dbId});
			if(thisRaid === null){
				session.endDialog("Deze Raid is inmiddels geannuleerd, sorry.");
				return;
			} else {
				var coming = thisRaid.coming;
				var user = users.findOne({"id": session.message.address.conversation.id});
				var thisUserName = user.name;
				coming.push(thisUserName);
				thisRaid.coming = coming;
				raids.update(thisRaid);
				session.endDialog("Oke " + thisUserName + ", tot dan!\r\n\r\n" +
					"Er zijn op dit moment " + coming.length + " spelers die zich hebben aangemeld.\r\n\r\n" +
					"Mocht je van gedachten veranderen, gebruik /raids");
			}
        }else{
            session.endDialog("Je komt niet naar deze Raid. Mocht je van gedachten veranderen, gebruik /raids ");
        }
    }
]);
bot.dialog('comingToRaidGroup', function(session){
   session.endDialog("Hey, " + senderUserName + " heeft een Raid gevonden. \r\n\r\n" +
       "- Pokémon: "+raidPokemon+"\r\n\r\n" +
       "- Locatie: "+raidLocation+"\r\n\r\n" +
       "- Starttijd: "+raidTime + "\r\n\r\n" +
	   "- " + getGmapsLocation(raidLocation) + "\r\n\r\n" +
       "Voeg mij persoonlijk toe (@PokemonGoRaidsHoogeveenBot) en geef daar aan of je komt!")
});


bot.dialog('reset', function (session) {
    users.findAndRemove({"id": session.message.address.conversation.id});
    session.endDialog('Je bent nu uitgeschreven voor deze bot. Opnieuw aanmelden? Zeg even /hallo :)');
}).triggerAction({ matches: /^\/reset/i });

var availableRaids = [];
var availableRaidsMap = [];
var lokiKey = null;
bot.dialog('raids', [
	function (session) {
		var user = users.findOne({"id": session.message.address.conversation.id});
		if (isGroup(user.name)) {
			return;
		}

		var date = new Date();
		var raidList = raids.find({
			"date-day": date.getDate(),
			"date-month": date.getMonth(),
			"date-year": date.getFullYear(),
			"date-hour": {"$gte": (date.getHours() - 2)}
		});

		if (raidList.length <= 0) {
			session.endDialog("Er zijn op dit moment geen Raids gemeld. Zie je er eentje in de buurt, voeg hem dan snel toe met het commando /nieuw");
		}else{
			availableRaids = [];
			availableRaidsMap = [];
			raidList.forEach(function(raid){
				var numberOfPlayers = (raid.coming.length).toString();
				var readable = raid.pokemon.substr(0,4) + ","+raid.location.substr(0,7)+ ","+raid.raidTime.substr(0,5)+","+numberOfPlayers.substr(0,2);
				availableRaids.push(readable);
				availableRaidsMap.push({"text": readable, "id": raid.$loki});
			});
			builder.Prompts.choice(session, "" +
				"De volgende Raids zijn de afgelopen 2 uur gemeld. " +
				"Kies een van de volgende Raids om je af- of aan te melden. \r\n", availableRaids);
		}
	},
	function (session, results) {
	lokiKey = null;
		availableRaidsMap.forEach(function (raid) {
			if(raid.text === results.response.entity){
				lokiKey = raid.id;
			}
		});
		if(lokiKey === null){
			session.endDialog("Foutmelding 2: er is iets fout gegaan, probeer het opnieuw.");
		}else{
			var currentRaid = raids.findOne({"$loki": lokiKey});
			if(currentRaid === null){
				session.endDialog("Deze Raid is inmiddels geannuleerd, sorry.");
				return;
			} else {
				var comingPlayers = "";
				if (currentRaid.coming.length > 0) {
					comingPlayers = "De volgende " + currentRaid.coming.length + " spelers zijn aanwezig bij deze Raid: ";
					currentRaid.coming.forEach(function (player) {
						comingPlayers = comingPlayers + player + ", ";
					});
					comingPlayers = comingPlayers.substr(0, comingPlayers.length - 2);
					comingPlayers = comingPlayers + "\r\n\r\n";
				}
				builder.Prompts.choice(session, comingPlayers + "Ben jij aanwezig bij deze " + currentRaid.pokemon + "-Raid?", RaidComingChoices);
			}
		}
	},
	function(session, results){
		var thisRaid = raids.findOne({"$loki": lokiKey});
		if(thisRaid === null){
			session.endDialog("Deze Raid is inmiddels geannuleerd, sorry.");
			return;
		}
		var coming = thisRaid.coming;
		var user = users.findOne({"id": session.message.address.conversation.id});
		var thisUserName = user.name;
		if(results.response.entity === RaidComingChoices[0]){
			if(!(coming.indexOf(thisUserName) > -1)){
				coming.push(thisUserName);
				thisRaid.coming = coming;
				raids.update(thisRaid);
				session.endDialog("Oke "+thisUserName+", tot dan!\r\n\r\n " +
					"Er zijn op dit moment "+thisRaid.coming.length+" spelers die zich hebben aangemeld.\r\n\r\n" +
					"Mocht je van gedachten veranderen, gebruik /raids");
			}else{
				session.endDialog("Je was al aangemeld voor deze raid, "+thisUserName+". Gebruik /raids om dit aan te passen.");
			}
		}else{
			var filtered = coming.filter(function(e){ return e !== thisUserName});
			thisRaid.coming = filtered;
			raids.update(thisRaid);
			session.endDialog("Je bent nu niet aangemeld voor deze Raid. Gebruik /raids om dit aan te passen.");
		}
	}
]).triggerAction({ matches: /^\/raids/i });

var availableRaids = [];
var availableRaidsMap = [];
var lokiKey = null;
bot.dialog('cancel', [
	function (session) {
		var user = users.findOne({"id": session.message.address.conversation.id});
		if (isGroup(user.name)) {
			return;
		}
		var date = new Date();
		var raidList = raids.find({
			"date-day": date.getDate(),
			"date-month": date.getMonth(),
			"date-year": date.getFullYear(),
			"date-hour": {"$gte": (date.getHours() - 2)},
			"userId": session.message.address.conversation.id
		});

		if (raidList.length <= 0) {
			session.endDialog("Je hebt op dit moment geen Raids gemeld. Zie je er eentje in de buurt, voeg hem dan snel toe met het commando /nieuw");
			return;
		}else{
			availableRaids = [];
			availableRaidsMap = [];
			raidList.forEach(function(raid){
				var readable = raid.pokemon.substr(0, 5) + ","+raid.location.substr(0,8)+ ","+raid.raidTime.substr(0,5);
				availableRaids.push(readable);
				availableRaidsMap.push({"text": readable, "id": raid.$loki});
			});
			availableRaids.push("Terug");
			builder.Prompts.choice(session, "" +
				"Je hebt de volgende Raids gemeld. " +
				"Tik op de Raid die je wil annuleren. Wil je niks annuleren, klik op _terug_. \r\n", availableRaids);
		}
	},
	function (session, results) {
	lokiKey = null;
		availableRaidsMap.forEach(function (raid) {
			if(raid.text === results.response.entity){
				lokiKey = raid.id;
				console.log(lokiKey);
			}
		});
		if(lokiKey === null){
			session.endDialog("Er zijn geen Raids geannuleerd.");
			return;
		}else{
			var allUsers = users.find({notify: true});
			var thisRaid = raids.findOne({"$loki": lokiKey});
			raidPokemon = thisRaid.pokemon;
			raidLocation = thisRaid.location;
			raidTime = thisRaid.raidTime;
			var coming = thisRaid.coming;
			allUsers.forEach(function (user) {
				if((coming.indexOf(user.name) > -1) || isGroup(user.name)){
					console.log(user);
					bot.beginDialog(user.address, '*:raidCancelled');
				}
			});
			raids.findAndRemove({"$loki": lokiKey});
			session.endDialog("Je heb je raid '"+results.response.entity+"' geannuleerd.");
		}
	}
]).triggerAction({ matches: /^\/annuleren/i });

bot.dialog('raidCancelled', function(session){
	session.endDialog("Let op: de Raid met "+raidPokemon+" bij "+raidLocation+" om "+raidTime+" is geannuleerd.")
});

bot.dialog('help', function (session) {
    var user = users.findOne({"id": session.message.address.conversation.id});
    if(isGroup(user.name)){
        return;
    }
    session.endDialog(HelpMessage);
}).triggerAction({ matches: /^\/help/i });

/**
 * Only for superAdmin
 */
bot.dialog('resetDatabaseUsers', function(session){
	var user = users.findOne({"id": session.message.address.conversation.id});
    if(isGroup(user.name)){
        return;
    }
    if(user.name === "Richh94") {
		users.chain().remove();
		session.endDialog("Database Users resetted!");
	}
	session.endDialog("Doe eens niet!");
}).triggerAction({ matches: /^\/resetDatabaseUsers/i });
bot.dialog('notifyEveryone', function (session) {
	var user = users.findOne({"id": session.message.address.conversation.id});
	if(isGroup(user.name)){
		return;
	}
	if(user.name === "Richh94") {
		var userResult = users.find({"notify": true});
		userResult.forEach(function(user){
			bot.beginDialog(user.address, '*:notifyEveryoneMessage');
		});
	}
}).triggerAction({ matches: /^\/notifyEveryone/i });
bot.dialog('notifyEveryoneMessage', function(session){
	session.endDialog("Hey, door wijzigingen aan deze bot is het nodig om alle data te verwijderen. " +
		"Je zal straks opnieuw je username in moeten vullen om weer meldingen te krijgen bij een Raid. " +
		"Ook groepen zullen opnieuw moeten worden toegevoegd. " +
		"Wil je na dit bericht minimaal 5 minuten wachten en daarna /start tegen me zeggen? ")
});

/**
 * Special admin functions
 */
bot.dialog('logUsers', function (session) {
	var user = users.findOne({"id": session.message.address.conversation.id});
	if(isGroup(user.name)){
		return;
	}

	if(isAdmin(user.name)) {
		var players = users.find({"notify": true});
		session.endDialog(JSON.stringify(players));
	}
}).triggerAction({ matches: /^\/logUsers/i });
bot.dialog('logRaids', function (session) {
	var user = users.findOne({"id": session.message.address.conversation.id});
	if(isGroup(user.name)){
		return;
	}

	if(isAdmin(user.name)) {
		var date = new Date();
		var raidList = raids.find({
			"date-day": date.getDate(),
			"date-month": date.getMonth(),
			"date-year": date.getFullYear()
		});
		session.endDialog(JSON.stringify(raidList));
	}
}).triggerAction({ matches: /^\/logRaids/i });
bot.dialog('resetDatabaseRaids', function(session){
	var user = users.findOne({"id": session.message.address.conversation.id});
	if(isGroup(user.name)){
		return;
	}
	if(isAdmin("Richh94")) {
		raids.chain().remove();
		session.endDialog("Database Raids resetted!");
	}
}).triggerAction({ matches: /^\/resetDatabaseRaids/i });
bot.dialog('cancelAdmin', [
	function (session) {
		var user = users.findOne({"id": session.message.address.conversation.id});
		if (isGroup(user.name) || !isAdmin(user.name)) {
			return;
		}

		var date = new Date();
		var raidList = raids.find({
			"date-day": date.getDate(),
			"date-month": date.getMonth(),
			"date-year": date.getFullYear(),
			"date-hour": {"$gte": (date.getHours() - 2)}
		});

		console.log(raidList);
		if (raidList.length <= 0) {
			session.endDialog("Er zijn op dit moment geen Raids gemeld.");
			return;
		}else{
			availableRaids = [];
			availableRaidsMap = [];
			raidList.forEach(function(raid){
				console.log(raid);
				var readable = raid.pokemon.substr(0, 5) + ","+raid.location.substr(0,8)+ ","+raid.raidTime.substr(0,5);
				availableRaids.push(readable);
				availableRaidsMap.push({"text": readable, "id": raid.$loki});
			});
			availableRaids.push("Terug");
			builder.Prompts.choice(session, "De volgende Raids zijn gemeld.\r\n\r\n" +
				"Tik op de Raid die je wil annuleren. Wil je niks annuleren, klik op _terug_. \r\n", availableRaids);
		}
	},
	function (session, results) {
		lokiKey = null;
		availableRaidsMap.forEach(function (raid) {
			if(raid.text === results.response.entity){
				lokiKey = raid.id;
			}
		});
		if(lokiKey === null){
			session.endDialog("Er zijn geen Raids geannuleerd.");
			return;
		}else{
			var allUsers = users.find({notify: true});
			var thisRaid = raids.findOne({"$loki": lokiKey});
			raidPokemon = thisRaid.pokemon;
			raidLocation = thisRaid.location;
			raidTime = thisRaid.raidTime;
			var coming = thisRaid.coming;
			allUsers.forEach(function (user) {
				if((coming.indexOf(user.name) > -1) || isGroup(user.name)){
					console.log(user);
					bot.beginDialog(user.address, '*:raidCancelled');
				}
			});
			raids.findAndRemove({"$loki": lokiKey});
			session.endDialog("Je heb je raid '"+results.response.entity+"' geannuleerd.");
		}
	}
]).triggerAction({ matches: /^\/adminAnnuleren/i });

function isGroup(username){
    return username === "GROEP";
}
function isAdmin(username) {
	return (Admins.indexOf(username) > -1);
}

//this is needed to start the server running...
if (useEmulator) {
    var restify = require('restify');
    var server = restify.createServer();
    server.listen(3978, function() {
        console.log('test bot endpoint at http://localhost:3978/api/messages');
    });
    server.post('/api/messages', connector.listen());
} else {
    var listener = connector.listen();
    var withLogging = function (context, req) {
        console.log = context.log;
        listener(context, req);
    }
    module.exports = {default: withLogging}
}